<?php

$_autojoin_servers =
    [
        ['irc.quakenet.org', 6667],
        //['irc.cc.tut.fi', 6667]
    ];

$_autojoin_user_info =
    [
        'nick' => 'BaseT',
        'altnick' => 'AltBase',
        'username' => 'baset',
        'realname' => '10/100Base-T Ports'
    ];

// No need for & since this will only be run at start
$_plugins['onload'][] = function() use ($_autojoin_servers, $_started, $_connect)
{
    if (!$_started)
    {
        foreach ($_autojoin_servers as $server)
        {
            $_connect($server[0], $server[1]);
        }
    }
};

$_plugins['onconnect'][] = function($id, $host, $port) use (&$_write, &$_autojoin_user_info)
{
    $_write($id, sprintf("NICK %s\r\n", $_autojoin_user_info['nick']));
    $_write($id, sprintf("USER %s 8 * :%s\r\n", $_autojoin_user_info['username'],
                                                $_autojoin_user_info['realname']));
};
