<?php

$_plugins['onload'][] = function()
{
    echo '[LOAD] Current memory usage: ', memory_get_usage(), PHP_EOL;
};

$_plugins['onforget'][] = function()
{
    echo '[FORGET] Current memory usage: ', memory_get_usage(), PHP_EOL;
};
