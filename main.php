<?php

declare(ticks = 1);

const PLUGIN_DIR = 'plugins';

// All IO streams
$_iostreams = [];

// All activated plugins
$_plugins = [];

$_started = false;

$_reload = function()
{
    static $require_reload = false;
    if (!func_num_args())
    {
        $ret = $require_reload;
        $require_reload = false;
        return $ret;
    }
    else
    {
        $require_reload = $require_reload || func_get_arg(0);
    }
};

// Available plugin hooks:
// onconnect
// oninput
// onoutput
// ondisconnect
// onload
// onforget

require 'utils.php';
$_load_code();
$_started = true;
while (true)
{
    $_loop();

    echo 'Executing scheduled reload...', PHP_EOL;
    $_forget_code();
    $_load_code();
}
